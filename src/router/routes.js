
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'recipe', component: () => import('pages/ComingSoon.vue') },
      { path: 'meal-type', component: () => import('pages/ComingSoon.vue') },
      { path: 'dish-type', component: () => import('pages/ComingSoon.vue') },
      { path: 'ingredient', component: () => import('pages/ComingSoon.vue') },
      { path: 'cooking-style', component: () => import('pages/ComingSoon.vue') },
      { path: 'diet-and-health', component: () => import('pages/ComingSoon.vue') },
      { path: 'seasonal', component: () => import('pages/ComingSoon.vue') },
      { path: 'chef-desk', component: () => import('pages/ComingSoon.vue') },
      { path: 'about-us', component: () => import('pages/ComingSoon.vue') },
      { path: 'special-collection', component: () => import('pages/ComingSoon.vue') },
    ],
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
