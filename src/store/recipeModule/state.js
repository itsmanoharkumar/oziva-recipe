export default function () {
    return {
        recipeList: [
            {
                id: 1,
                title: 'Grilled Tri-Tip with Oregon Herb Rub',
                image: 'https://images.media-allrecipes.com/userphotos/300x300/239754.jpg',
                description: 'I have loved tri-tip (bottom sirloin) ever since my father-in-law in Santa Barbara' +
                    ' introduced me to it. I love the taste of the original Santa Maria rub, but this is even better.',
                ingredient: [
                    '1 tablespoon salt',
                    '1 ½ teaspoons garlic salt',
                    '½ teaspoon celery salt',
                    '¼ teaspoon ground black pepper',
                    '¼ teaspoon onion powder',
                    '¼ teaspoon paprika',
                    '¼ teaspoon dried dill',
                    '¼ teaspoon dried sage',
                    '¼ teaspoon crushed dried rosemary',
                    '1 (2 1/2 pound) beef tri-tip roast'
                ],
                method: [
                    `Mix together the salt, garlic salt, celery salt, black pepper, onion powder, paprika, dill, sage, and rosemary in a bowl. Store in an airtight container at room temperature until ready to use.`,
                    `Use a damp towel to lightly moisten the roast with water, then pat with the prepared rub. Refrigerate for a minimum of 2 hours, up to overnight, for the flavors to fully come together.`,
                    `Preheat an outdoor grill for high heat and lightly oil grate.`,
                    `Place the roast onto the preheated grill and quickly cook until brown on all sides to sear the meat, then remove. Reset the grill for medium-low indirect heat (if using charcoal, move coals to the outside edges of the grill pit).`,
                    `Return the roast to the grill, and cook, turning occasionally, until the desired degree of doneness has been reached, about 1 1/2 hours for medium-well. Remove from the grill and cover with aluminum foil. Allow to rest for 10 minutes before carving across the grain in thin slices to serve.`,
                ],
                prepTime: '1 hour',
                cookingTime: '2 hour',
                difficultyLevel: 'Hard'
            }, {
                id: 2,
                title: 'Greek Lemon Chicken and Potatoes',
                image: 'https://images.media-allrecipes.com/userphotos/300x300/4539284.jpg',
                description: 'Chef John\'s Greek Lemon Chicken and Potatoes are roasted until the the chicken skin is…',
                ingredient: [
                    '1 tablespoon salt',
                    '1 ½ teaspoons garlic salt',
                    '½ teaspoon celery salt',
                    '¼ teaspoon ground black pepper',
                    '¼ teaspoon onion powder',
                    '¼ teaspoon paprika',
                    '¼ teaspoon dried dill',
                    '¼ teaspoon dried sage',
                    '¼ teaspoon crushed dried rosemary',
                    '1 (2 1/2 pound) beef tri-tip roast'
                ],
                 method: [
                    `Mix together the salt, garlic salt, celery salt, black pepper, onion powder, paprika, dill, sage, and rosemary in a bowl. Store in an airtight container at room temperature until ready to use.`,
                    `Use a damp towel to lightly moisten the roast with water, then pat with the prepared rub. Refrigerate for a minimum of 2 hours, up to overnight, for the flavors to fully come together.`,
                    `Preheat an outdoor grill for high heat and lightly oil grate.`,
                    `Place the roast onto the preheated grill and quickly cook until brown on all sides to sear the meat, then remove. Reset the grill for medium-low indirect heat (if using charcoal, move coals to the outside edges of the grill pit).`,
                    `Return the roast to the grill, and cook, turning occasionally, until the desired degree of doneness has been reached, about 1 1/2 hours for medium-well. Remove from the grill and cover with aluminum foil. Allow to rest for 10 minutes before carving across the grain in thin slices to serve.`,
                ],
                prepTime: '2 hour',
                cookingTime: '1 hour',
                difficultyLevel: 'Easy'
            }, {
                id: 3,
                title: 'Instant Pot® Salisbury Steak with Onion',
                image: 'https://images.media-allrecipes.com/userphotos/300x300/5678662.jpg',
                description: 'Next time you are craving Salisbury steak like mom used to make, you can cook it in your…',
                ingredient: [
                    '1 tablespoon salt',
                    '1 ½ teaspoons garlic salt',
                    '½ teaspoon celery salt',
                    '¼ teaspoon ground black pepper',
                    '¼ teaspoon onion powder',
                    '¼ teaspoon paprika',
                    '¼ teaspoon dried dill',
                    '¼ teaspoon dried sage',
                    '¼ teaspoon crushed dried rosemary',
                    '1 (2 1/2 pound) beef tri-tip roast'
                ],
                 method: [
                    `Mix together the salt, garlic salt, celery salt, black pepper, onion powder, paprika, dill, sage, and rosemary in a bowl. Store in an airtight container at room temperature until ready to use.`,
                    `Use a damp towel to lightly moisten the roast with water, then pat with the prepared rub. Refrigerate for a minimum of 2 hours, up to overnight, for the flavors to fully come together.`,
                    `Preheat an outdoor grill for high heat and lightly oil grate.`,
                    `Place the roast onto the preheated grill and quickly cook until brown on all sides to sear the meat, then remove. Reset the grill for medium-low indirect heat (if using charcoal, move coals to the outside edges of the grill pit).`,
                    `Return the roast to the grill, and cook, turning occasionally, until the desired degree of doneness has been reached, about 1 1/2 hours for medium-well. Remove from the grill and cover with aluminum foil. Allow to rest for 10 minutes before carving across the grain in thin slices to serve.`,
                ],
                prepTime: '3 hour',
                cookingTime: '4 hour',
                difficultyLevel: 'Hard'
            }, {
                id: 4,
                title: 'Grilled Tri-Tip with Oregon Herb Rub',
                image: 'https://images.media-allrecipes.com/userphotos/300x300/964194.jpg',
                description: 'This classic baked macaroni and cheese recipe is easy and delicious. Cheese, butter, milk,…',
                ingredient: [
                    '1 tablespoon salt',
                    '1 ½ teaspoons garlic salt',
                    '½ teaspoon celery salt',
                    '¼ teaspoon ground black pepper',
                    '¼ teaspoon onion powder',
                    '¼ teaspoon paprika',
                    '¼ teaspoon dried dill',
                    '¼ teaspoon dried sage',
                    '¼ teaspoon crushed dried rosemary',
                    '1 (2 1/2 pound) beef tri-tip roast'
                ],
                 method: [
                    `Mix together the salt, garlic salt, celery salt, black pepper, onion powder, paprika, dill, sage, and rosemary in a bowl. Store in an airtight container at room temperature until ready to use.`,
                    `Use a damp towel to lightly moisten the roast with water, then pat with the prepared rub. Refrigerate for a minimum of 2 hours, up to overnight, for the flavors to fully come together.`,
                    `Preheat an outdoor grill for high heat and lightly oil grate.`,
                    `Place the roast onto the preheated grill and quickly cook until brown on all sides to sear the meat, then remove. Reset the grill for medium-low indirect heat (if using charcoal, move coals to the outside edges of the grill pit).`,
                    `Return the roast to the grill, and cook, turning occasionally, until the desired degree of doneness has been reached, about 1 1/2 hours for medium-well. Remove from the grill and cover with aluminum foil. Allow to rest for 10 minutes before carving across the grain in thin slices to serve.`,
                ],
                prepTime: '4 hour',
                cookingTime: '6 hour',
                difficultyLevel: 'Medium'
            }, {
                id: 5,
                title: 'Baked Macaroni and Cheese',
                image: 'https://images.media-allrecipes.com/userphotos/300x300/3810781.jpg',
                description: 'This ground beef Stroganoff is full of flavor, but wallet-friendly and easy enough for a…',
                ingredient: [
                    '1 tablespoon salt',
                    '1 ½ teaspoons garlic salt',
                    '½ teaspoon celery salt',
                    '¼ teaspoon ground black pepper',
                    '¼ teaspoon onion powder',
                    '¼ teaspoon paprika',
                    '¼ teaspoon dried dill',
                    '¼ teaspoon dried sage',
                    '¼ teaspoon crushed dried rosemary',
                    '1 (2 1/2 pound) beef tri-tip roast'
                ],
                 method: [
                    `Mix together the salt, garlic salt, celery salt, black pepper, onion powder, paprika, dill, sage, and rosemary in a bowl. Store in an airtight container at room temperature until ready to use.`,
                    `Use a damp towel to lightly moisten the roast with water, then pat with the prepared rub. Refrigerate for a minimum of 2 hours, up to overnight, for the flavors to fully come together.`,
                    `Preheat an outdoor grill for high heat and lightly oil grate.`,
                    `Place the roast onto the preheated grill and quickly cook until brown on all sides to sear the meat, then remove. Reset the grill for medium-low indirect heat (if using charcoal, move coals to the outside edges of the grill pit).`,
                    `Return the roast to the grill, and cook, turning occasionally, until the desired degree of doneness has been reached, about 1 1/2 hours for medium-well. Remove from the grill and cover with aluminum foil. Allow to rest for 10 minutes before carving across the grain in thin slices to serve.`,
                ],
                prepTime: '1 hour',
                cookingTime: '2 hour',
                difficultyLevel: 'Hard'
            }, {
                id: 6,
                title: 'Deb\'s General Tso\'s Chicken',
                image: 'https://images.media-allrecipes.com/userphotos/300x300/1017268.jpg',
                description: 'You\'ll enjoy the spicy sweet soy sauce with ginger and garlic flavors in this Chinese…',
                ingredient: [
                    '1 tablespoon salt',
                    '1 ½ teaspoons garlic salt',
                    '½ teaspoon celery salt',
                    '¼ teaspoon ground black pepper',
                    '¼ teaspoon onion powder',
                    '¼ teaspoon paprika',
                    '¼ teaspoon dried dill',
                    '¼ teaspoon dried sage',
                    '¼ teaspoon crushed dried rosemary',
                    '1 (2 1/2 pound) beef tri-tip roast'
                ],
                 method: [
                    `Mix together the salt, garlic salt, celery salt, black pepper, onion powder, paprika, dill, sage, and rosemary in a bowl. Store in an airtight container at room temperature until ready to use.`,
                    `Use a damp towel to lightly moisten the roast with water, then pat with the prepared rub. Refrigerate for a minimum of 2 hours, up to overnight, for the flavors to fully come together.`,
                    `Preheat an outdoor grill for high heat and lightly oil grate.`,
                    `Place the roast onto the preheated grill and quickly cook until brown on all sides to sear the meat, then remove. Reset the grill for medium-low indirect heat (if using charcoal, move coals to the outside edges of the grill pit).`,
                    `Return the roast to the grill, and cook, turning occasionally, until the desired degree of doneness has been reached, about 1 1/2 hours for medium-well. Remove from the grill and cover with aluminum foil. Allow to rest for 10 minutes before carving across the grain in thin slices to serve.`,
                ],
                prepTime: '2 hour',
                cookingTime: '1 hour',
                difficultyLevel: 'Easy'
            }, {
                id: 7,
                title: 'Quick Fish Tacos',
                image: 'https://images.media-allrecipes.com/userphotos/300x300/5822211.jpg',
                description: 'Tilapia fillets are pan-fried in a jalapeno-infused oil and served with a quick cabbage slaw. For…',
                ingredient: [
                    '1 tablespoon salt',
                    '1 ½ teaspoons garlic salt',
                    '½ teaspoon celery salt',
                    '¼ teaspoon ground black pepper',
                    '¼ teaspoon onion powder',
                    '¼ teaspoon paprika',
                    '¼ teaspoon dried dill',
                    '¼ teaspoon dried sage',
                    '¼ teaspoon crushed dried rosemary',
                    '1 (2 1/2 pound) beef tri-tip roast'
                ],
                 method: [
                    `Mix together the salt, garlic salt, celery salt, black pepper, onion powder, paprika, dill, sage, and rosemary in a bowl. Store in an airtight container at room temperature until ready to use.`,
                    `Use a damp towel to lightly moisten the roast with water, then pat with the prepared rub. Refrigerate for a minimum of 2 hours, up to overnight, for the flavors to fully come together.`,
                    `Preheat an outdoor grill for high heat and lightly oil grate.`,
                    `Place the roast onto the preheated grill and quickly cook until brown on all sides to sear the meat, then remove. Reset the grill for medium-low indirect heat (if using charcoal, move coals to the outside edges of the grill pit).`,
                    `Return the roast to the grill, and cook, turning occasionally, until the desired degree of doneness has been reached, about 1 1/2 hours for medium-well. Remove from the grill and cover with aluminum foil. Allow to rest for 10 minutes before carving across the grain in thin slices to serve.`,
                ],
                prepTime: '3 hour',
                cookingTime: '3 hour',
                difficultyLevel: 'Hard'
            }, {
                id: 8,
                title: 'Detroit-Style Pizza',
                image: 'https://images.media-allrecipes.com/userphotos/300x300/4573295.jpg',
                description: 'Chef John seals the edges of this Detroit-style pizza with plenty of cheese, which fries as it…',
                ingredient: [
                    '1 tablespoon salt',
                    '1 ½ teaspoons garlic salt',
                    '½ teaspoon celery salt',
                    '¼ teaspoon ground black pepper',
                    '¼ teaspoon onion powder',
                    '¼ teaspoon paprika',
                    '¼ teaspoon dried dill',
                    '¼ teaspoon dried sage',
                    '¼ teaspoon crushed dried rosemary',
                    '1 (2 1/2 pound) beef tri-tip roast'
                ],
                 method: [
                    `Mix together the salt, garlic salt, celery salt, black pepper, onion powder, paprika, dill, sage, and rosemary in a bowl. Store in an airtight container at room temperature until ready to use.`,
                    `Use a damp towel to lightly moisten the roast with water, then pat with the prepared rub. Refrigerate for a minimum of 2 hours, up to overnight, for the flavors to fully come together.`,
                    `Preheat an outdoor grill for high heat and lightly oil grate.`,
                    `Place the roast onto the preheated grill and quickly cook until brown on all sides to sear the meat, then remove. Reset the grill for medium-low indirect heat (if using charcoal, move coals to the outside edges of the grill pit).`,
                    `Return the roast to the grill, and cook, turning occasionally, until the desired degree of doneness has been reached, about 1 1/2 hours for medium-well. Remove from the grill and cover with aluminum foil. Allow to rest for 10 minutes before carving across the grain in thin slices to serve.`,
                ],
                prepTime: '4 hour',
                cookingTime: '1 hour',
                difficultyLevel: 'Medium'
            },
        ],
        currentRecipe: {}
    }
}
