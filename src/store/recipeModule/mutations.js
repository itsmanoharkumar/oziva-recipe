export function ADD_RECIPE(state, payload) {
  state.recipeList.push(payload)
}

export function SET_CURRENT_RECIPE(state, payload) {
  state.currentRecipe = payload
}

export function DELETE_RECIPE(state, id) {
    state.recipeList = state.recipeList.filter((recipe) => {
        return recipe.id !== id
    })
}
