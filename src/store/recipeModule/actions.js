export async function addRecipe({ commit }, payload) {
  try {
    await commit('ADD_RECIPE', payload)
  } catch (err) {
    throw err
  }
}

export async function setCurrentRecipe({ commit }, payload) {
  try {
    await commit('SET_CURRENT_RECIPE', payload)
  } catch (err) {
    throw err
  }
}

export async function deleteRecipe({ commit }, payload) {
  try {
    await commit('DELETE_RECIPE', payload)
  } catch (err) {
    throw err
  }
}
